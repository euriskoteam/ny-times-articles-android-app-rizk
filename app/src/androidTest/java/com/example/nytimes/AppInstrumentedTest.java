package com.example.nytimes;

import android.view.View;

import androidx.annotation.IdRes;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.nytimes.activities.ArticlesActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.AllOf.allOf;


/**
 * This class was designed & implemented to test recycler view using instrumented test
 */
@RunWith(AndroidJUnit4.class)
public class AppInstrumentedTest {

    @Rule
    public ActivityScenarioRule<ArticlesActivity> activityTestRule =
            new ActivityScenarioRule<>(ArticlesActivity.class);

    public static ViewAction withCustomConstraints(final ViewAction action, final Matcher<View> constraints) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return constraints;
            }

            @Override
            public String getDescription() {
                return action.getDescription();
            }

            @Override
            public void perform(UiController uiController, View view) {
                action.perform(uiController, view);
            }
        };
    }


    @Test
    public void instrumentedTest() {
        if(getCountFromRecyclerView(R.id.rvArticles)>4) {

            // Click the item on position 1 in recycler view
            onView(withId(R.id.rvArticles))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));

            // Go to details activity and sleep for 3000 ms
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //go back to home activity
            pressBack();

            // Click the item on position 2 in recycler view
            onView(withId(R.id.rvArticles))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));

            // Go to details activity and sleep for 3000 ms
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //go back to home activity
            pressBack();

            // Click the item on position 3 in recycler view
            onView(withId(R.id.rvArticles))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));

            // Go to details activity and sleep for 3000 ms
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //go back to home activity
            pressBack();
        }
        //Swipe refresh layout to refresh data
        onView(withId(R.id.srlArticles))
                .perform(withCustomConstraints(swipeDown(), isDisplayingAtLeast(85)));

        //sleep for 5000 ms then exit
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static int getCountFromRecyclerView(@IdRes int RecyclerViewId) {
        final int[] COUNT = {0};
        Matcher matcher = new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                COUNT[0] = ((RecyclerView) item).getAdapter().getItemCount();
                return true;
            }
            @Override
            public void describeTo(Description description) {}
        };
        onView(allOf(withId(RecyclerViewId),isDisplayed())).check(matches(matcher));
        return COUNT[0];
    }
}
