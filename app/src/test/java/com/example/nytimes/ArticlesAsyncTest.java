package com.example.nytimes;

import android.app.Activity;

import com.example.nytimes.async.GetArticlesAsync;
import com.example.nytimes.beans.ArticleBean;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.fail;

/**
 * This class was designed & implemented to test get articles async task
 */
@RunWith(RobolectricTestRunner.class)
public class ArticlesAsyncTest {

        Activity activity;
        GetArticlesAsync getArticlesAsync;


        @Before
        public void setUp() {
            activity = Robolectric.setupActivity(Activity.class);
        }

        @Test
        public void getFormattedDate_Test_ReturnTrue_1(){
            getArticlesAsync = new GetArticlesAsync(new GetArticlesAsync.OnFinishListener() {
                @Override
                public void onSuccess(Object object) {
                    assertTrue(true);
                }

                @Override
                public void onError(Object msg) {
                    fail();
                }
            });
            getArticlesAsync.execute();
        }


}
