package com.example.nytimes;

import com.example.nytimes.utils.DateUtils;

import org.junit.Test;

import java.util.Locale;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * This class was designed & implemented to test formatted date function
 */
public class FormatDateTest {

    @Test
    public void getFormattedDate_Test_ReturnTrue_1(){
        assertTrue(DateUtils.getFormattedDate(null,DateUtils.PUBLISHED_DATE_INPUT_FORMAT,
                DateUtils.PUBLISHED_DATE_OUTPUT_FORMAT, Locale.US).equals(""));
    }

    @Test
    public void getFormattedDate_Test_ReturnTrue_2(){
        assertTrue(DateUtils.getFormattedDate("",DateUtils.PUBLISHED_DATE_INPUT_FORMAT,
                DateUtils.PUBLISHED_DATE_OUTPUT_FORMAT, Locale.US).equals(""));
    }

    @Test
    public void getFormattedDate_Test_ReturnTrue_3(){
        assertTrue(DateUtils.getFormattedDate("2019-11-23",DateUtils.PUBLISHED_DATE_INPUT_FORMAT,
                DateUtils.PUBLISHED_DATE_OUTPUT_FORMAT, Locale.US).equals("Nov 23, 2019"));
    }

    @Test
    public void getFormattedDate_Test_ReturnFalse_1(){
        assertFalse(DateUtils.getFormattedDate("2019-11-23",DateUtils.PUBLISHED_DATE_INPUT_FORMAT,
                DateUtils.PUBLISHED_DATE_OUTPUT_FORMAT, Locale.US).equals("Nov 23"));
    }

    @Test
    public void getFormattedDate_Test_ReturnFalse_2(){
        assertFalse(DateUtils.getFormattedDate("2019-11-23",DateUtils.PUBLISHED_DATE_INPUT_FORMAT,
                DateUtils.PUBLISHED_DATE_OUTPUT_FORMAT, Locale.US).equals("2019-11-23"));
    }

}
