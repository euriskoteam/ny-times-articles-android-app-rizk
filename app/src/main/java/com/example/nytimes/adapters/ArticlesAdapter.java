package com.example.nytimes.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nytimes.events.ArticleClickedEvent;
import com.example.nytimes.R;
import com.example.nytimes.beans.ArticleBean;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * This class was designed & implemented to list the articles
 */
public class ArticlesAdapter extends RecyclerView.Adapter {

    /**
     * The variable used to store the list of articles
     */
    private ArrayList<ArticleBean> articleBeans;
    /**
     * The variable used to inflate the article item layout
     */
    private LayoutInflater inflater;
    /**
     * The variable used to load the image from url
     */
    private ImageLoader imageLoader;
    /**
     * The variable used to set options for image loader
     */
    private DisplayImageOptions imageOptions;

    public ArticlesAdapter(Activity activity) {
        articleBeans = new ArrayList<>();
        inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(activity));
        imageOptions = new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.IN_SAMPLE_INT).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.item_article, viewGroup, false);
        return new ArticleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ArticleHolder holder = (ArticleHolder) viewHolder;
        final ArticleBean articleBean = articleBeans.get(position);

        holder.tvArticleTitle.setText(articleBean.getTitle());
        holder.tvCreatedBy.setText(articleBean.getByLine());
        holder.tvPublishedDate.setText(articleBean.getPublishedDate());

        holder.rlArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new ArticleClickedEvent(articleBean));
            }
        });

        imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(articleBean.getImageUrl(), holder.ivArticleImage, imageOptions);

    }

    @Override
    public int getItemCount() {
        return articleBeans.size();
    }

    /**
     * This method was designed & implemented to assign the list of articles to adapter
     *
     * @param articleBeans list of articles
     */
    public void setArticles(ArrayList<ArticleBean> articleBeans) {
        this.articleBeans = articleBeans;
        notifyDataSetChanged();
    }

    class ArticleHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivArticleImage;
        private TextView tvArticleTitle, tvCreatedBy, tvPublishedDate;
        private RelativeLayout rlArticle;

        public ArticleHolder(@NonNull View itemView) {
            super(itemView);
            ivArticleImage = itemView.findViewById(R.id.ivArticleImage);
            tvArticleTitle = itemView.findViewById(R.id.tvArticleTitle);
            tvCreatedBy = itemView.findViewById(R.id.tvCreatedBy);
            tvPublishedDate = itemView.findViewById(R.id.tvPublishedDate);
            rlArticle = itemView.findViewById(R.id.rlArticle);
        }
    }
}
