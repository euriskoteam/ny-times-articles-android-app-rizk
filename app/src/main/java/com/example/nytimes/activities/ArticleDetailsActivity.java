package com.example.nytimes.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.nytimes.events.ArticleLoadedEvent;
import com.example.nytimes.R;
import com.example.nytimes.beans.ArticleBean;
import com.example.nytimes.viewmodels.ArticleDetailsViewModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * This class was designed & implemented to handle article details screen
 */
public class ArticleDetailsActivity extends AppCompatActivity {

    /**
     * The variable used to display article title
     */
    private TextView tvArticleTitle;
    /**
     * The variable used to display article description
     */
    private TextView tvDescription;
    /**
     * The variable used to display article published date
     */
    private TextView tvPublishedDate;
    /**
     * The variable used to display created by
     */
    private TextView tvCreatedBy;
    /**
     * The variable used to display article image
     */
    private ImageView ivArticleImage;
    /**
     * The variable used to store article attributes
     */
    private ArticleBean articleBean;
    /**
     * The variable used to get data for UI
     */
    private ArticleDetailsViewModel articleDetailsViewModel;
    /**
     * The variable used to load image from URL
     */
    private ImageLoader imageLoader;
    /**
     * The variable used to add image display options
     */
    private DisplayImageOptions imageOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);
        EventBus.getDefault().register(this);
        setupViews();
        implementViews();
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
        imageOptions = new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.IN_SAMPLE_INT).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();

    }

    /**
     * This method was designed & implemented to handle setup and bind views
     */
    private void setupViews() {
        tvArticleTitle = findViewById(R.id.tvArticleTitle);
        tvDescription = findViewById(R.id.tvDescription);
        ivArticleImage = findViewById(R.id.ivArticleImage);
        tvPublishedDate = findViewById(R.id.tvPublishedDate);
        tvCreatedBy = findViewById(R.id.tvCreatedBy);
    }

    /**
     * This method was designed & implemented to implement views functionalities
     */
    private void implementViews() {
        articleDetailsViewModel = new ArticleDetailsViewModel();
        articleDetailsViewModel.loadArticleData(getIntent());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(ArticleLoadedEvent event) {
        articleBean = event.articleBean;
        fillData();
    }

    /**
     * This method was designed & implemented to fill the data in the screen
     */
    private void fillData() {
        tvArticleTitle.setText(articleBean.getTitle());
        tvPublishedDate.setText(articleBean.getPublishedDate());
        tvDescription.setText(articleBean.getDescription());
        tvCreatedBy.setText(articleBean.getByLine());
        imageLoader = ImageLoader.getInstance();
        if (!articleBean.getImageUrl().isEmpty()) {
            imageLoader.displayImage(articleBean.getImageUrl(), ivArticleImage, imageOptions);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
