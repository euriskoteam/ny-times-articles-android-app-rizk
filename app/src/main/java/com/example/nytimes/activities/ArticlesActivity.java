package com.example.nytimes.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.nytimes.R;
import com.example.nytimes.adapters.ArticlesAdapter;
import com.example.nytimes.events.ArticleClickedEvent;
import com.example.nytimes.events.ArticleListAvailableEvent;
import com.example.nytimes.utils.IntentVars;
import com.example.nytimes.viewmodels.ArticlesViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * This class was designed & implemented to handle home screen
 */
public class ArticlesActivity extends AppCompatActivity {

    /**
     * The variable used to display list of articles
     */
    private RecyclerView rvArticles;
    /**
     * The variable used to save activity instance
     */
    private Activity activity;
    /**
     * The variable used to fill article item data
     */
    private ArticlesAdapter articlesAdapter;
    /**
     * The variable used to refresh articles list
     */
    private SwipeRefreshLayout srlArticles;
    /**
     * The variable used to get data for UI
     */
    private ArticlesViewModel articlesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setupViews();

        implementViews();
    }

    /**
     * This method was designed & implemented to handle setup and bind views
     */
    private void setupViews() {
        activity = this;
        rvArticles = findViewById(R.id.rvArticles);
        srlArticles = findViewById(R.id.srlArticles);
        EventBus.getDefault().register(activity);
    }

    /**
     * This method was designed & implemented to implement views functionalities
     */
    private void implementViews() {
        srlArticles.setRefreshing(true);
        articlesViewModel = new ArticlesViewModel();
        articlesViewModel.getArticleBeans(activity);

        articlesAdapter = new ArticlesAdapter(activity);
        rvArticles.setAdapter(articlesAdapter);

        srlArticles.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlArticles.setRefreshing(true);
                articlesViewModel.getArticleBeans(activity);
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(ArticleListAvailableEvent event) {
        System.out.println("onSuccess>>>>onMessage");
        srlArticles.setRefreshing(false);
        articlesAdapter.setArticles(event.articleBeans);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(ArticleClickedEvent event) {
        Intent intent = new Intent(activity, ArticleDetailsActivity.class);
        intent.putExtra(IntentVars.ARTICLE_BEAN_BUNDLE, event.articleBean);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
