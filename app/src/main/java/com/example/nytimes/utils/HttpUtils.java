package com.example.nytimes.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class was designed & implemented to handle home screen
 */
public class HttpUtils {

    /**
     * This method was designed & implemented to get the response from url
     *
     * @param urlString the url
     * @return json result in a string
     */
    public static String getHttps(String urlString) {
        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(urlString);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(httpConn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            httpConn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

}
