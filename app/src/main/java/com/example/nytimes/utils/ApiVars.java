package com.example.nytimes.utils;


/**
 * This class was designed & implemented to contain all api vars
 */
public class ApiVars {

    /**
     * The variable used to store the url
     */
    public final static String ARTICLES_URL = "http://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/7.json?api-key=Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5";

    /**
     * The variable used to store the success response status
     */
    public final static String RESPONSE_STATUS_OK = "OK";
}
