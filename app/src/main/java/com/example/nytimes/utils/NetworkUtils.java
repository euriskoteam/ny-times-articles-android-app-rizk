package com.example.nytimes.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * This class was designed & implemented to handle network issues
 */
public class NetworkUtils {
    /**
     * This method was designed & implemented to check if internet connection is available
     *
     * @param activity The variable used to save activity instance
     * @return boolean if there is network connection or no
     */
    public static boolean isNetworkAvailable(Activity activity) {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        try {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

}
