package com.example.nytimes.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This class was designed & implemented to handle date converting
 */
public class DateUtils {

    /**
     * The variable used to save input format
     */
    public static final String PUBLISHED_DATE_INPUT_FORMAT = "yyyy-MM-dd";
    /**
     * The variable used to save output format
     */
    public static final String PUBLISHED_DATE_OUTPUT_FORMAT = "MMM d, yyyy";


    /**
     * This method was designed & implemented to change a formatted date to a new format date
     *
     * @param strDate the date string
     * @param inputFormat the input date format
     * @param outputFormat the output format
     * @param locale locale
     * @return string date in destiny format
     */
    public static String getFormattedDate(String strDate, String inputFormat, String outputFormat, Locale locale) {

        if (strDate==null || strDate.isEmpty()){
            return "";
        }
        SimpleDateFormat df;
        df = new SimpleDateFormat(inputFormat, locale);
        Date date = null;
        try {
            date = df.parse(strDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        df = new SimpleDateFormat(outputFormat, locale);
        return df.format(date);

    }

}
