package com.example.nytimes.utils;

/**
 * This class was designed & implemented to contain all intent vars
 */
public class IntentVars {

    /**
     * The variable used to send article bean between activities
     */
    public static final String ARTICLE_BEAN_BUNDLE = "article_bean";

}
