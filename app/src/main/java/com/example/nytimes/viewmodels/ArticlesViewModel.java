package com.example.nytimes.viewmodels;

import android.app.Activity;
import android.widget.Toast;

import androidx.lifecycle.ViewModel;

import com.example.nytimes.events.ArticleListAvailableEvent;
import com.example.nytimes.R;
import com.example.nytimes.async.GetArticlesAsync;
import com.example.nytimes.beans.ArticleBean;
import com.example.nytimes.utils.NetworkUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * This class was designed & implemented as a view model for Home activity
 */
public class ArticlesViewModel extends ViewModel {

    /**
     * The variable used to store the list of articles
     */
    private ArrayList<ArticleBean> articleBeans;

    /**
     * This method was designed & implemented to get articles from URL
     *
     * @param activity activity instance
     */
    public void getArticleBeans(final Activity activity) {
        GetArticlesAsync getArticlesAsync;
        if(NetworkUtils.isNetworkAvailable(activity)) {
            getArticlesAsync = new GetArticlesAsync(new GetArticlesAsync.OnFinishListener() {
                @Override
                public void onSuccess(Object object) {
                    articleBeans = (ArrayList<ArticleBean>) object;
                    EventBus.getDefault().post(new ArticleListAvailableEvent(articleBeans));
                }

                @Override
                public void onError(Object msg) {
                    Toast.makeText(activity, activity.getString(R.string.msg_error_fetching), Toast.LENGTH_LONG).show();
                    EventBus.getDefault().post(new ArticleListAvailableEvent(new ArrayList<ArticleBean>()));
                }
            });
            getArticlesAsync.execute();
        }else{
            EventBus.getDefault().post(new ArticleListAvailableEvent(new ArrayList<ArticleBean>()));
            Toast.makeText(activity,activity.getString(R.string.msg_error_connection),Toast.LENGTH_LONG).show();
        }
    }
}
