package com.example.nytimes.viewmodels;

import android.content.Intent;

import androidx.lifecycle.ViewModel;

import com.example.nytimes.events.ArticleLoadedEvent;
import com.example.nytimes.beans.ArticleBean;
import com.example.nytimes.utils.DateUtils;
import com.example.nytimes.utils.IntentVars;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

/**
 * This class was designed & implemented as a view model for article details activity
 */
public class ArticleDetailsViewModel extends ViewModel {

    /**
     * The variable used to store article attributes
     */
    private ArticleBean articleBean;

    /**
     * This method was designed & implemented to get the article attributes
     *
     * @param intent intent
     */
    public void loadArticleData(Intent intent) {
        if (intent != null) {
            articleBean = (ArticleBean) intent.getExtras().getSerializable(IntentVars.ARTICLE_BEAN_BUNDLE);
            String dateFormatted = DateUtils.getFormattedDate(articleBean.getPublishedDate(),
                    DateUtils.PUBLISHED_DATE_INPUT_FORMAT,
                    DateUtils.PUBLISHED_DATE_OUTPUT_FORMAT, Locale.US);
            articleBean.setPublishedDate(dateFormatted);
            EventBus.getDefault().post(new ArticleLoadedEvent(articleBean));
        }
    }

}
