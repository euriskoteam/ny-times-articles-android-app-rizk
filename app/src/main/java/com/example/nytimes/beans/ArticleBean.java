package com.example.nytimes.beans;


import java.io.Serializable;

/**
 * This class was designed & implemented to declare Article attributes
 */
public class ArticleBean implements Serializable {
    /**
     * The variable used to save the title of the article
     */
    private String title = "";
    /**
     * The variable used to save the type of the article
     */
    private String type = "";
    /**
     * The variable used to save the description of the article
     */
    private String description = "";
    /**
     * The variable used to save the published date of the article
     */
    private String publishedDate = "";
    /**
     * The variable used to save the source of the article
     */
    private String source = "";
    /**
     * The variable used to save the section of the article
     */
    private String section = "";
    /**
     * The variable used to save by line of the article
     */
    private String byLine = "";
    /**
     * The variable used to save image url
     */
    private String imageUrl = "";


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getByLine() {
        return byLine;
    }

    public void setByLine(String byLine) {
        this.byLine = byLine;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
