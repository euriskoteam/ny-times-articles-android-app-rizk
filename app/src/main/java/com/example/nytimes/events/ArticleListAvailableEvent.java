package com.example.nytimes.events;

import com.example.nytimes.beans.ArticleBean;

import java.util.ArrayList;

/**
 * This class was designed & implemented handle list ready event
 */
public class ArticleListAvailableEvent {

    public ArrayList<ArticleBean> articleBeans;

    public ArticleListAvailableEvent(ArrayList<ArticleBean> articleBeans) {
        this.articleBeans = articleBeans;
    }

}
