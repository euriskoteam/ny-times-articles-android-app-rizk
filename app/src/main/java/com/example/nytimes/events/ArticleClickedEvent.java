package com.example.nytimes.events;

import com.example.nytimes.beans.ArticleBean;

/**
 * This class was designed & implemented handle recycler click event
 */
public class ArticleClickedEvent {
    public ArticleBean articleBean;

    public ArticleClickedEvent(ArticleBean articleBean) {
        this.articleBean = articleBean;
    }

}
