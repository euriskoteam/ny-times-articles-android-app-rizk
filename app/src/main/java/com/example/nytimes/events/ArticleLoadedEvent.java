package com.example.nytimes.events;

import com.example.nytimes.beans.ArticleBean;

/**
 * This class was designed & implemented handle article loaded event
 */
public class ArticleLoadedEvent {

    public ArticleBean articleBean;

    public ArticleLoadedEvent(ArticleBean articleBean) {
        this.articleBean = articleBean;
    }

}
