package com.example.nytimes.async;

import android.os.AsyncTask;

import com.example.nytimes.beans.ArticleBean;
import com.example.nytimes.utils.ApiVars;
import com.example.nytimes.utils.HttpUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This class was designed & implemented to get the list of articles
 */
public class GetArticlesAsync extends AsyncTask<Object, Object, Object> {

    /**
     * The variable used to save the json result
     */
    private String jsonResult;
    /**
     * The variable used to store the list of articles
     */
    private ArrayList<ArticleBean> articleBeans;
    /**
     * The variable used to setup the on finish listener
     */
    private OnFinishListener mOnFinishListener;
    /**
     * The variable used to store the result in json object
     */
    private JSONObject jsonObjectResult;

    public GetArticlesAsync(OnFinishListener mOnFinishListener) {
        this.mOnFinishListener = mOnFinishListener;
        articleBeans = new ArrayList<>();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            jsonResult = HttpUtils.getHttps(ApiVars.ARTICLES_URL);
            jsonObjectResult = new JSONObject(jsonResult);
            if (getStatusCode(jsonObjectResult).equals(ApiVars.RESPONSE_STATUS_OK)) {
                articleBeans = getArticleBeans();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (getStatusCode(jsonObjectResult).equals(ApiVars.RESPONSE_STATUS_OK)) {
            mOnFinishListener.onSuccess(articleBeans);
        } else {
            mOnFinishListener.onError("");
        }
    }

    /**
     * This method was designed & implemented to get the status code of the response
     *
     * @param jsonObject json object of the response
     * @return status code
     */
    private String getStatusCode(JSONObject jsonObject) {
        String statusCode = "";

        try {
            if (jsonObject != null && jsonObject.has("status") && !jsonObject.isNull("status")) {
                statusCode = jsonObject.getString("status");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return statusCode;
    }

    /**
     * This method was designed & implemented to get articles list from fetched response
     *
     * @return list of articles
     */
    private ArrayList<ArticleBean> getArticleBeans() {
        ArrayList<ArticleBean> articleBeans = new ArrayList<>();
        JSONArray resultsArray;
        try {
            if (jsonObjectResult.has("results") && !jsonObjectResult.isNull("results")) {
                resultsArray = jsonObjectResult.getJSONArray("results");
                for (int i = 0; i < resultsArray.length(); i++) {
                    JSONObject articleObject = resultsArray.getJSONObject(i);
                    ArticleBean articleBean = new ArticleBean();
                    if (articleObject.has("section") && !articleObject.isNull("section")) {
                        articleBean.setSection(articleObject.getString("section"));
                    }
                    if (articleObject.has("type") && !articleObject.isNull("type")) {
                        articleBean.setType(articleObject.getString("type"));
                    }
                    if (articleObject.has("title") && !articleObject.isNull("title")) {
                        articleBean.setTitle(articleObject.getString("title"));
                    }
                    if (articleObject.has("abstract") && !articleObject.isNull("abstract")) {
                        articleBean.setDescription(articleObject.getString("abstract"));
                    }
                    if (articleObject.has("published_date") && !articleObject.isNull("published_date")) {
                        articleBean.setPublishedDate(articleObject.getString("published_date"));
                    }
                    if (articleObject.has("source") && !articleObject.isNull("source")) {
                        articleBean.setSource(articleObject.getString("source"));
                    }
                    if (articleObject.has("byline") && !articleObject.isNull("byline")) {
                        articleBean.setByLine(articleObject.getString("byline"));
                    }
                    if (articleObject.has("media") && !articleObject.isNull("media")) {
                        JSONArray mediaArray = articleObject.getJSONArray("media");
                        if (mediaArray.length() > 0) {
                            JSONObject mediaObject = mediaArray.getJSONObject(0);
                            if (mediaObject.has("media-metadata") && !mediaObject.isNull("media-metadata")) {
                                JSONArray imagesArray = mediaObject.getJSONArray("media-metadata");
                                if (imagesArray.length() > 0) {
                                    //Here I chose the first picture from the list of pictures
                                    JSONObject imageObject = imagesArray.getJSONObject(0);
                                    if (imageObject.has("url") && !imageObject.isNull("url")) {
                                        articleBean.setImageUrl(imageObject.getString("url"));
                                    }
                                }
                            }
                        }
                    }
                    articleBeans.add(articleBean);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return articleBeans;
    }

    public interface OnFinishListener {
        void onSuccess(Object object);

        void onError(Object msg);
    }

}
