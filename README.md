# Description

* Simple application to show New York Times popular articles.
* Created using Java.
* The application follows the MVVM design patterns



# How to clone and run the app 

* In Android Studio choose File -> New -> Project from version control -> Git.
* Paste the bitbucket URL then click clone.
* Sync and rebuild the app.



# Unit Testing

### FormatDateTest

* Right Click on the FortmatDateTest class and click on "Run FormatDateTest".
* Or you can open this class, and click run on each function of this class.



### ArticlesAsyncTest

* Right Click on the ArticlesAsyncTest class and click on "Run ArticlesAsyncTest".
* This test is created to validate NY time articles API.



### AppInstrumentedTest

* Right Click on the AppInstrumentedTest class and click on "Run AppInstrumentedTest".
* How It Works:
  * First we check if the size of the recyclerview is greater then 5.
    * if it's greater then 5: We click on position 1 of the recycler view, we go to the details activity, then we go back to the home activity after 3 seconds and repeat what we have done for position 2 and 3.
  * We go back to the home activity and swipe the layout to refresh the articles, we wait for 5 seconds and close application.



# Authors

*** Rizk Mouawad ***





